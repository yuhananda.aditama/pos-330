package id.bootcamp.pos.di

import android.content.Context
import id.bootcamp.pos.data.ExampleRepository
import id.bootcamp.pos.data.PosRepository
import id.bootcamp.pos.data.local.ExampleSharedPreference
import id.bootcamp.pos.data.local.room.ExampleDatabase
import id.bootcamp.pos.data.local.room.PosDatabase
import id.bootcamp.pos.data.remote.retrofit.ApiConfig

object Injection {
    fun provideExampleRepository(context: Context):ExampleRepository{
        val exampleApiService = ApiConfig.getRegresApiService()
        val exampleDatabase = ExampleDatabase.getInstance(context)
        val exampleDao = exampleDatabase.exampleDao()
        val exampleSharedPreference = ExampleSharedPreference(context)
        return ExampleRepository.getInstance(exampleApiService,exampleDao,exampleSharedPreference)
    }

    fun providePosRepository(context: Context): PosRepository{
        val posDatabase = PosDatabase.getInstance(context)
        val categoryDao = posDatabase.categoryDao()
        return PosRepository.getInstance(categoryDao)
    }
}