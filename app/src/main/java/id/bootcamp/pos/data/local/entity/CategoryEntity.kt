package id.bootcamp.pos.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "category")
class CategoryEntity {

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @field:ColumnInfo(name = "initial")
    var initial: String = ""

    @field:ColumnInfo(name = "name")
    var name: String = ""

    @field:ColumnInfo(name = "active")
    var active: Boolean = true

    @field:ColumnInfo(name = "created_by")
    var createdBy: Long = 0

    @field:ColumnInfo(name = "created_on")
    var createdOn: Date = Date()

    @field:ColumnInfo(name = "modified_by")
    var modifiedBy: Long? = null

    @field:ColumnInfo(name = "modified_on")
    var modifiedOn: Date? = null

    @field:ColumnInfo(name = "deleted_by")
    var deletedBy: Long? = null

    @field:ColumnInfo(name = "deleted_on")
    var deletedOn: Date? = null

    @field:ColumnInfo(name = "is_delete")
    var isDelete: Boolean = false
}