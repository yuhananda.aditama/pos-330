package id.bootcamp.pos.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "m_user")
class M_User {

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @field:ColumnInfo(name = "biodata_id")
    var biodataId: Long? = null

    @field:ColumnInfo(name = "role_id")
    var roleId: Long? = null

    @field:ColumnInfo(name = "email")
    var email: String? = null

    @field:ColumnInfo(name = "password")
    var password: String? = null

    @field:ColumnInfo(name = "login_attempt")
    var loginAttempt: Int? = null

    @field:ColumnInfo(name = "is_locked")
    var isLocked: Boolean? = null

    @field:ColumnInfo(name = "last_login")
    var lastLogin: Date? = null

    @field:ColumnInfo(name = "created_by")
    var createdBy: Long = 0

    @field:ColumnInfo(name = "created_on")
    var createdOn: Date = Date()

    @field:ColumnInfo(name = "modified_by")
    var modifiedBy: Long? = null

    @field:ColumnInfo(name = "modified_on")
    var modifiedOn: Date? = null

    @field:ColumnInfo(name = "deleted_by")
    var deletedBy: Long? = null

    @field:ColumnInfo(name = "deleted_on")
    var deletedOn: Date? = null

    @field:ColumnInfo(name = "is_delete")
    var isDelete: Boolean = false
}