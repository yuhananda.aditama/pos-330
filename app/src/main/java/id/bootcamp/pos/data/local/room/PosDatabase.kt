package id.bootcamp.pos.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import id.bootcamp.pos.data.local.entity.CategoryEntity
import id.bootcamp.pos.data.local.entity.ExampleEntity
import id.bootcamp.pos.data.local.entity.M_Biodata
import id.bootcamp.pos.data.local.entity.M_User

@TypeConverters(DateConverter::class)
@Database(
    entities = [M_User::class, M_Biodata::class, CategoryEntity::class],
    version = 2
)
abstract class PosDatabase : RoomDatabase() {

    abstract fun categoryDao(): CategoryDao

    companion object {
        @Volatile
        private var instance: PosDatabase? = null
        fun getInstance(context: Context): PosDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    PosDatabase::class.java, "pos.db"
                )
                    //.createFromAsset("database/example.db")
                    .fallbackToDestructiveMigration()
                    .build()
            }
    }
}