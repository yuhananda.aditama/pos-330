package id.bootcamp.pos.data

import id.bootcamp.pos.data.local.ExampleSharedPreference
import id.bootcamp.pos.data.local.entity.CategoryEntity
import id.bootcamp.pos.data.local.room.CategoryDao
import id.bootcamp.pos.data.local.room.ExampleDao
import id.bootcamp.pos.data.remote.retrofit.RegresApiService
import id.bootcamp.pos.ui.data.CategoryData
import id.bootcamp.pos.ui.data.ExampleData
import id.bootcamp.pos.ui.data.ExamplePage
import java.util.Date

class PosRepository private constructor(
    val categoryDao: CategoryDao
) {

    suspend fun getCategoryListFromDao(): List<CategoryData> {
        val dataListDao: List<CategoryEntity> = categoryDao.getCategoryListFromDb()

        //Cara Mapping
        //Mapping, ubah tipe data list dari CategoryEntity ke CategoryData
//        val categoryDataList : List<CategoryData> =
//            dataListDao.map {
//                CategoryData(
//                    it.id,
//                    it.initial,
//                    it.name,
//                    it.active
//                )
//            }

        //Cara pake perulangan
        val categoryDataList = ArrayList<CategoryData>()
        for (i in 0..dataListDao.lastIndex) {
            val categoryData = CategoryData(
                dataListDao[i].id,
                dataListDao[i].initial,
                dataListDao[i].name,
                dataListDao[i].active
            )
            categoryDataList.add(categoryData)
        }

        return categoryDataList
    }

    suspend fun insertCategoryToDao(categoryData: CategoryData): CategoryData {
        val initialCount = categoryDao.countInitialWhere(categoryData.initial)
        if (initialCount > 0) {
            val data = CategoryData()
            data.isSuccess = false
            data.errorView = "initial"
            data.errorMessage = "Initial sudah dipakai!"
            return data
        }

        val nameCount = categoryDao.countNameWhere(categoryData.name)
        if (nameCount > 0) {
            val data = CategoryData()
            data.isSuccess = false
            data.errorView = "name"
            data.errorMessage = "Name sudah dipakai!"
            return data
        }

        try {
            val catEntity = CategoryEntity()
            catEntity.initial = categoryData.initial
            catEntity.name = categoryData.name
            catEntity.active = categoryData.active
            //hardcode dulu, nanti kalo udah ada sistem login. diubah
            catEntity.createdBy = 1
            catEntity.createdOn = Date()
            categoryDao.insertCategoryToDb(catEntity)

            val data = CategoryData()
            data.isSuccess = true
            return data
        } catch (e: Exception) {
            e.printStackTrace()
            val data = CategoryData()
            data.isSuccess = false
            data.errorView = ""
            data.errorMessage = e.message ?: ""
            return data
        }
    }

    suspend fun updateCategoryToDao(categoryData: CategoryData): CategoryData {
        val initialCount =
            categoryDao.countInitialEditWhere(categoryData.initial, categoryData.id)
        if (initialCount > 0) {
            val data = CategoryData()
            data.isSuccess = false
            data.errorView = "initial"
            data.errorMessage = "Initial sudah dipakai!"
            return data
        }

        val nameCount =
            categoryDao.countNameEditWhere(categoryData.name, categoryData.id)
        if (nameCount > 0) {
            val data = CategoryData()
            data.isSuccess = false
            data.errorView = "name"
            data.errorMessage = "Name sudah dipakai!"
            return data
        }

        try {
            val catEntity = categoryDao.getCategoryById(categoryData.id)
            catEntity.initial = categoryData.initial
            catEntity.name = categoryData.name
            catEntity.active = categoryData.active
            //hardcode dulu, nanti kalo udah ada sistem login. diubah
            catEntity.modifiedBy = 1
            catEntity.modifiedOn = Date()
            categoryDao.updateCategoryToDb(catEntity)

            val data = CategoryData()
            data.isSuccess = true
            return data
        } catch (e: Exception) {
            e.printStackTrace()
            val data = CategoryData()
            data.isSuccess = false
            data.errorView = ""
            data.errorMessage = e.message ?: ""
            return data
        }
    }

    companion object {
        @Volatile
        private var instance: PosRepository? = null
        fun getInstance(
            categoryDao: CategoryDao
        ): PosRepository =
            instance ?: synchronized(this) {
                instance ?: PosRepository(categoryDao)
            }.also { instance = it }
    }
}