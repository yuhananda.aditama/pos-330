package id.bootcamp.pos.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "m_biodata")
class M_Biodata {

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @field:ColumnInfo(name = "fullname")
    var fullname: String? = null

    @field:ColumnInfo(name = "mobile_phone")
    var mobilePhone: String? = null

    @field:ColumnInfo(name = "image")
    var image: String? = null

    @field:ColumnInfo(name = "image_path")
    var imagePath: String? = null

    @field:ColumnInfo(name = "created_by")
    var createdBy: Long = 0

    @field:ColumnInfo(name = "created_on")
    var createdOn: Date = Date()

    @field:ColumnInfo(name = "modified_by")
    var modifiedBy: Long? = null

    @field:ColumnInfo(name = "modified_on")
    var modifiedOn: Date? = null

    @field:ColumnInfo(name = "deleted_by")
    var deletedBy: Long? = null

    @field:ColumnInfo(name = "deleted_on")
    var deletedOn: Date? = null

    @field:ColumnInfo(name = "is_delete")
    var isDelete: Boolean = false
}