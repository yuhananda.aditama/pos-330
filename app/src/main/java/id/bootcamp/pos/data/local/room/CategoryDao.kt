package id.bootcamp.pos.data.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import id.bootcamp.pos.data.local.entity.CategoryEntity

@Dao
interface CategoryDao {

    @Query("SELECT * FROM CATEGORY WHERE IS_DELETE = 0")//0 false,  1 true
    suspend fun getCategoryListFromDb(): List<CategoryEntity>

    @Query("SELECT COUNT(*) FROM CATEGORY WHERE INITIAL = :initial")
    suspend fun countInitialWhere(initial: String) : Int

    @Query("SELECT COUNT(*) FROM CATEGORY WHERE NAME = :name")
    suspend fun countNameWhere(name: String) : Int

    @Insert
    suspend fun insertCategoryToDb(categoryEntity: CategoryEntity)

    @Query("SELECT COUNT(*) FROM CATEGORY WHERE INITIAL = :initial AND ID != :id")
    suspend fun countInitialEditWhere(initial: String, id: Long) : Int

    @Query("SELECT COUNT(*) FROM CATEGORY WHERE NAME = :name AND ID != :id")
    suspend fun countNameEditWhere(name: String, id: Long) : Int

    @Update
    suspend fun updateCategoryToDb(categoryEntity: CategoryEntity)

    @Query("SELECT * FROM CATEGORY WHERE IS_DELETE = 0 AND ID = :id")
    suspend fun getCategoryById(id : Long) : CategoryEntity

    @Query("UPDATE CATEGORY SET IS_DELETE = 1 WHERE ID = :id")
    suspend fun deleteCategoryById(id : Long)
}