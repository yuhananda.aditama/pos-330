package id.bootcamp.pos.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.bootcamp.pos.R
import id.bootcamp.pos.databinding.ActivityCategoryBinding
import id.bootcamp.pos.ui.adapter.CategoryAdapter
import id.bootcamp.pos.ui.data.CategoryData
import id.bootcamp.pos.ui.viewmodel.CategoryViewModel
import id.bootcamp.pos.ui.viewmodel.PosViewModelFactory

class CategoryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCategoryBinding
    private lateinit var viewModel: CategoryViewModel
    private lateinit var mAdapter: CategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(
            this,
            PosViewModelFactory.getInstance(this)
        ).get(CategoryViewModel::class.java)

        setupRecyclerView()
        setupObserverLiveData()
        setupListener()

        //Ambil data dari viewModel
        viewModel.getListCategoryFromRepository()
    }

    private fun setupRecyclerView() {
        binding.rvCategory.layoutManager = LinearLayoutManager(this)
        mAdapter = CategoryAdapter(ArrayList())
        binding.rvCategory.adapter = mAdapter

        mAdapter.onCategoryItemListener = object : CategoryAdapter.OnCategoryItemListener {
            override fun onEditClick(categoryData: CategoryData) {
                val editCategoryFragment = EditCategoryFragment()
                val bundle = Bundle()
                bundle.putParcelable("data",categoryData)
                editCategoryFragment.arguments = bundle
                val fragmentManager = supportFragmentManager
                editCategoryFragment.show(fragmentManager,
                    EditCategoryFragment::class.java.simpleName)

                editCategoryFragment.onEditCategoryListener = object : EditCategoryFragment.OnEditCategoryListener{
                    override fun onEditSuccess() {
                        Toast.makeText(
                            this@CategoryActivity, "Edit Success",
                            Toast.LENGTH_SHORT
                        ).show()
                        viewModel.getListCategoryFromRepository()
                    }
                }
            }

            override fun onDeleteClick(categoryData: CategoryData) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun setupObserverLiveData() {
        viewModel.listCatLiveData.observe(this) {
            if (it != null) {
                updateUI(it)
                viewModel.listCatLiveData.postValue(null)
            }
        }
    }

    private fun updateUI(dataList: List<CategoryData>) {
        mAdapter.resetList(dataList)
        binding.srlCategory.isRefreshing = false
    }

    private fun setupListener() {
        binding.srlCategory.setOnRefreshListener {
            viewModel.getListCategoryFromRepository()
        }

        binding.fabAddCategory.setOnClickListener {
            val addCategoryFragment = AddCategoryFragment()
            val fragmentManager = supportFragmentManager
            addCategoryFragment.show(
                fragmentManager, AddCategoryFragment::class.java.simpleName
            )
            addCategoryFragment.onCreateCategoryListener =
                object : AddCategoryFragment.OnCreateCategoryListener {
                    override fun onCreateSuccess() {
                        Toast.makeText(
                            this@CategoryActivity, "Save Success",
                            Toast.LENGTH_SHORT
                        ).show()
                        viewModel.getListCategoryFromRepository()
                    }
                }
        }
    }
}