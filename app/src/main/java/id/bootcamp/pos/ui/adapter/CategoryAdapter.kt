package id.bootcamp.pos.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import id.bootcamp.pos.R
import id.bootcamp.pos.ui.data.CategoryData
import id.bootcamp.pos.ui.data.ExampleData

class CategoryAdapter(val categoryList: ArrayList<CategoryData?>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_LOADING = 1
    }

    var onCategoryItemListener: OnCategoryItemListener? = null
    interface OnCategoryItemListener {
        fun onEditClick(categoryData: CategoryData)
        fun onDeleteClick(categoryData: CategoryData)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvInitial = itemView.findViewById<TextView>(R.id.tvInitial)
        val tvName = itemView.findViewById<TextView>(R.id.tvName)
        val cbActive = itemView.findViewById<CheckBox>(R.id.cbActive)
        val btnEdit = itemView.findViewById<Button>(R.id.btnEdit)
        val btnDelete = itemView.findViewById<Button>(R.id.btnDelete)
    }

    class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pgLoading = itemView.findViewById<ProgressBar>(R.id.progressBar)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
            return ViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
            return LoadingHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val data : CategoryData? = categoryList[position]
            if (data != null) {
                holder.tvInitial.text = data.initial
                holder.tvName.text = data.name
                holder.cbActive.isChecked = data.active

                holder.btnEdit.setOnClickListener {
                    onCategoryItemListener?.onEditClick(data)
                }

                holder.btnDelete.setOnClickListener {
                    onCategoryItemListener?.onDeleteClick(data)
                }
            }
        }
    }


    override fun getItemCount(): Int {
        return categoryList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (categoryList[position] == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    fun resetList(dataList : List<CategoryData>){
        categoryList.clear()
        categoryList.addAll(ArrayList(dataList))
        notifyDataSetChanged()
    }
}