package id.bootcamp.pos.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.pos.data.PosRepository
import id.bootcamp.pos.ui.data.CategoryData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CategoryViewModel(private val posRepository: PosRepository) : ViewModel() {

    val listCatLiveData = MutableLiveData<List<CategoryData>>()
    val categoryLiveData = MutableLiveData<CategoryData>()

    fun getListCategoryFromRepository() = viewModelScope.launch {
//        val dataList : List<CategoryData> = getFakeData()
        delay(1000)
        val dataList: List<CategoryData> = posRepository.getCategoryListFromDao()
        listCatLiveData.postValue(dataList)
    }

    fun insertCategoryToRepository(categoryData: CategoryData) = viewModelScope.launch {
        delay(1000)
        val data = posRepository.insertCategoryToDao(categoryData)
        categoryLiveData.postValue(data)
    }

    fun updateCategoryToRepository(categoryData: CategoryData) = viewModelScope.launch {
        delay(1000)
        val data = posRepository.updateCategoryToDao(categoryData)
        categoryLiveData.postValue(data)
    }

    private fun getFakeData(): List<CategoryData> {
        val dataList = ArrayList<CategoryData>()
        for (i in 1..20) {
            val data: CategoryData = CategoryData()
            data.id = i.toLong()
            data.initial = "I$i"
            data.name = "Name $i"
            data.active = i % 2 == 0
            dataList.add(data)
        }
        return dataList
    }

}