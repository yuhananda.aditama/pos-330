package id.bootcamp.pos.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.pos.databinding.FragmentAddCategoryBinding
import id.bootcamp.pos.databinding.FragmentEditCategoryBinding
import id.bootcamp.pos.ui.data.CategoryData
import id.bootcamp.pos.ui.viewmodel.CategoryViewModel
import id.bootcamp.pos.ui.viewmodel.PosViewModelFactory

class EditCategoryFragment : DialogFragment() {
    private lateinit var binding: FragmentEditCategoryBinding
    private lateinit var viewModel: CategoryViewModel
    private var dataAwal : CategoryData? = null

    var onEditCategoryListener: OnEditCategoryListener? = null
    interface OnEditCategoryListener {
        fun onEditSuccess()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentEditCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, PosViewModelFactory.getInstance(requireContext())).get(
            CategoryViewModel::class.java
        )

        dataAwal = arguments?.getParcelable<CategoryData>("data")
        val myData = dataAwal
        if (dataAwal != null){
            binding.tilInitial.editText?.setText(dataAwal?.initial)
            binding.tilName.editText?.setText(dataAwal?.name)
            binding.cbActive.isChecked = dataAwal?.active ?: false
        }

        setupListener()
        setupObserverLiveData()
    }


    private fun setupListener() {
        binding.btnCancel.setOnClickListener {
            dialog?.dismiss()
        }

        binding.btnCreate.setOnClickListener {
            //Validasi Form
            val txtInitial = binding.tilInitial.editText?.text.toString().trim()
            val txtName = binding.tilName.editText?.text.toString().trim()
            val isActive = binding.cbActive.isChecked

            var isFormValid = true

            if (txtInitial.isEmpty()) {
                binding.tilInitial.error = "Initial tidak boleh kosong"
                isFormValid = false
            } else if (txtInitial.length > 10) {
                binding.tilInitial.error = "Initial tidak boleh lebih dari 10 karakter"
                isFormValid = false
            } else {
                binding.tilInitial.error = null
            }

            if (txtName.isEmpty()) {
                binding.tilName.error = "Name tidak boleh kosong"
                isFormValid = false
            } else if (txtName.length > 50) {
                binding.tilName.error = "Name tidak boleh lebih dari 50 karakter"
                isFormValid = false
            } else {
                binding.tilName.error = null
            }

            if (isFormValid) {
                //Save data
                val data = CategoryData(
                    id = dataAwal?.id ?: 0,
                    initial = txtInitial,
                    name = txtName,
                    active = isActive
                )
                viewModel.updateCategoryToRepository(data)
            }
        }
    }

    private fun setupObserverLiveData() {
        viewModel.categoryLiveData.observe(this) {
            if (it != null) {
                updateUI(it)
                viewModel.categoryLiveData.postValue(null)
            }
        }
    }

    private fun updateUI(categoryData: CategoryData) {
        if (categoryData.isSuccess) {
//            Toast.makeText(
//                requireContext(), "Save Success",
//                Toast.LENGTH_SHORT
//            ).show()
            onEditCategoryListener?.onEditSuccess()
            dialog?.dismiss()
        } else {
            if (categoryData.errorView == "initial") {
                binding.tilInitial.error = categoryData.errorMessage
            } else if (categoryData.errorView == "name") {
                binding.tilName.error = categoryData.errorMessage
            } else {
                Toast.makeText(
                    requireContext(),
                    categoryData.errorMessage, Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        //Code agar popup dialognya full width
        dialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        );
    }
}