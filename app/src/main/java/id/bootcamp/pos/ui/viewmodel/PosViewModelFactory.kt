package id.bootcamp.pos.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.pos.data.ExampleRepository
import id.bootcamp.pos.data.PosRepository
import id.bootcamp.pos.di.Injection

class PosViewModelFactory private constructor(private val posRepository: PosRepository) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CategoryViewModel::class.java)) {
            return CategoryViewModel(posRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    companion object {
        @Volatile
        private var instance: PosViewModelFactory? = null
        fun getInstance(context: Context): PosViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: PosViewModelFactory(Injection.providePosRepository(context))
            }.also { instance = it }
    }
}