package id.bootcamp.pos.ui.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

//Object dengan data-data yang dibutuhkan oleh UI
@Parcelize
class CategoryData (
    var id: Long = 0,
    var initial: String = "",
    var name: String = "",
    var active: Boolean = false
) :Parcelable {
    var isSuccess = false //true jika berhasil, false jika gagal
    var errorMessage = "" //Nampung error message
    var errorView = "" //Kasih tau, error bagian mana? misal : initial, name, dll
}